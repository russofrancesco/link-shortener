import { useLayoutEffect, useState } from "react";
import ShortenerBar from "./ShortenerBar";
import UserLinkSheet from "./UserLinkSheet";
import { shortenLink } from "@src/shortenLink";
import Typography from "@mui/joy/Typography";
import "./App.css";

export interface LinkPair {
  original: string;
  short: string;
}

function App() {
  const [linkPairs, setLinkPairs] = useState<LinkPair[]>([]);
  const [errorMsg, setErrorMsg] = useState("");

  // On page load/reload, read link pairs from localStorage
  // (employ useLayoutEffect to execute synchronously, before
  // the browser paints the UI; in this way there is no flickering)
  useLayoutEffect(() => {
    const newLinkPairs = localStorage.getItem("linkPairs");
    if (newLinkPairs) {
      setLinkPairs(JSON.parse(newLinkPairs));
    }
  }, []);

  // On shorten request, await the API call, then update link pairs
  async function handleShortenReq(link: string): Promise<void> {
    const { ok, original, short, error } = await shortenLink(link);
    if (!ok) {
      setErrorMsg(error);
      return;
    }
    const newLinkPairs = [{ original, short }, ...linkPairs];
    setLinkPairs(newLinkPairs);
    localStorage.setItem("linkPairs", JSON.stringify(newLinkPairs));
    setErrorMsg("");
  }

  return (
    <div>
      <h1>Link Shortener</h1>
      <Typography sx={{ my: "1rem", color: "red" }}>{errorMsg}</Typography>
      <ShortenerBar onClick={handleShortenReq} />
      <UserLinkSheet linkPairs={linkPairs} />
    </div>
  );
}

export default App;
