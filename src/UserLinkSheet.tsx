import UserLink from "@src/UserLink";
import { LinkPair } from "@src/App";
import Box from "@mui/joy/Box";

interface UserLinkSheetProps {
  linkPairs: LinkPair[];
}

function UserLinkSheet({ linkPairs }: UserLinkSheetProps) {
  const rows: JSX.Element[] = [];

  linkPairs.forEach((pair) => {
    rows.push(<UserLink key={pair.short} original={pair.original} short={pair.short} />);
  });

  return <Box sx={{ display: "flex", flexDirection: "column", gap: "0.5rem" }}>{rows}</Box>;
}

export default UserLinkSheet;
