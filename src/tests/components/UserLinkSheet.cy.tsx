import UserLinkSheet from "@src/UserLinkSheet";
import { LinkPair } from "@src/App";

describe("<UserLinkSheet>", () => {
  it("renders a list of UserLink components", () => {
    const linkPairs: LinkPair[] = [
      { original: "https://startpage.com", short: "https://sho.rt/G03f5A" },
      { original: "https://google.com", short: "https://sho.rt/72h1ad" },
    ];

    cy.mount(<UserLinkSheet linkPairs={linkPairs} />);
    cy.findByRole("link", { name: linkPairs[0].original }).should("have.attr", "href", linkPairs[0].original);
    cy.findByRole("link", { name: linkPairs[1].original }).should("have.attr", "href", linkPairs[1].original);
    cy.findByRole("link", { name: linkPairs[0].short }).should("have.attr", "href", linkPairs[0].short);
    cy.findByRole("link", { name: linkPairs[1].short }).should("have.attr", "href", linkPairs[1].short);
  });
});
