import ShortenerBar from "@src/ShortenerBar";

// Write your tests here!
describe("ShortenerBar", () => {
  it("renders the input and Shorten button", () => {
    cy.mount(<ShortenerBar onClick={() => Promise.resolve()} />);
    cy.findByPlaceholderText("Shorten a URL...");
    cy.findByRole("button").should("have.text", "Shorten");
  });

  it("calls the callback when the Shorten button is clicked", () => {
    // "as" gives a name (an "alias") to the spy function
    const onClickSpy = cy.spy().as("onClickSpy");

    cy.mount(<ShortenerBar onClick={onClickSpy} />);
    cy.findByRole("button").click();
    cy.get("@onClickSpy").should("have.been.called");
  });
});
