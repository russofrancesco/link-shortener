import UserLink from "@src/UserLink";

describe("<UserLink>", () => {
  const original = "https://example.com";
  const short = "https://sho.rt/x9kLS3";

  it("shows the original link, shortened link and Copy button", () => {
    cy.mount(<UserLink original={original} short={short} />);
    cy.findByRole("link", { name: original }).should("have.attr", "href", original);
    cy.findByRole("link", { name: short }).should("have.attr", "href", short);
    cy.findByRole("button").should("have.text", "Copy");
  });

  it("changes Copy button text on button click and on button blur", () => {
    cy.mount(<UserLink original={original} short={short} />);
    cy.findByRole("button").click().should("have.text", "Copied!").blur().should("have.text", "Copy");
  });
});
