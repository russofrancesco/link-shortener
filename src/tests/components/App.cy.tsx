import App from "@src/App";

// Note: works with Electron but not with Chrome because
// the antivirus blocks the fetch request before it's even
// replaced by Cypress.
describe("<App>", () => {
  it("shortens a valid URL", () => {
    cy.intercept("GET", "https://api.shrtco.de/v2/shorten?url=https://startpage.com", {
      statusCode: 201,
      body: {
        ok: true,
        result: {
          original_link: "https://startpage.com",
          full_short_link: "https://sho.rt/x9kLS3",
        },
      },
    });
    cy.mount(<App />);
    cy.findByPlaceholderText("Shorten a URL...").type("https://startpage.com");
    cy.findByRole("button").click();
    cy.findByRole("link", { name: "https://startpage.com" });
    cy.findByRole("link", { name: "https://sho.rt/x9kLS3" });
  });

  it("shows an error message if the URL is invalid", () => {
    cy.intercept("GET", "https://api.shrtco.de/v2/shorten?url=a", {
      statusCode: 400,
      body: {
        ok: false,
        error: "Invalid URL",
      },
    });
    cy.mount(<App />);
    cy.findByPlaceholderText("Shorten a URL...").type("a");
    cy.findByRole("button").click();
    cy.findByText("Invalid URL");
  });

  it("shows an error message if no URL was specified in the input", () => {
    cy.intercept("GET", "https://api.shrtco.de/v2/shorten?url=", {
      statusCode: 400,
      body: {
        ok: false,
        error: "No URL specified",
      },
    });
    cy.mount(<App />);
    cy.findByRole("button").click();
    cy.findByText("No URL specified");
  });
});
