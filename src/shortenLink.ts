export interface ShortenLinkRes {
  ok: boolean;
  original: string;
  short: string;
  error: string;
}

export async function shortenLink(link: string): Promise<ShortenLinkRes> {
  const res = await fetch(`https://api.shrtco.de/v2/shorten?url=${link}`);
  const data = await res.json();

  if (data.ok) {
    return { ok: true, original: data.result.original_link, short: data.result.full_short_link, error: data.error };
  } else {
    return { ok: false, original: "", short: "", error: data.error };
  }
}
