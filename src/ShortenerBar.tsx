import { useState } from "react";
import Button from "@mui/joy/Button";
import Input from "@mui/joy/Input";

interface ShortenerBarProps {
  onClick: (link: string) => Promise<void>;
}

function ShortenerBar({ onClick }: ShortenerBarProps) {
  const [value, setValue] = useState("");
  const [isLoading, setIsLoading] = useState(false);

  async function handleClick(event: React.FormEvent<HTMLFormElement>) {
    event.preventDefault();
    setIsLoading(true);
    await onClick(value);
    setIsLoading(false);
    setValue("");
  }

  return (
    <form onSubmit={handleClick} style={{ display: "flex", marginBottom: "1rem" }}>
      <Input
        type="text"
        name="url"
        value={value}
        onChange={(e) => setValue(e.target.value)}
        placeholder="Shorten a URL..."
        sx={{ flexGrow: 1, mr: "0.5rem" }}
      />
      <Button type="submit" loading={isLoading}>
        Shorten
      </Button>
    </form>
  );
}

export default ShortenerBar;
