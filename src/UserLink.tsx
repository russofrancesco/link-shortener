import { useState } from "react";
import Button from "@mui/joy/Button";
import Link from "@mui/joy/Link";
import Box from "@mui/joy/Box";
import { styled } from "@mui/joy/styles";

interface UserLinkProps {
  original: string;
  short: string;
}

function UserLink({ original, short }: UserLinkProps) {
  const [btnText, setBtnText] = useState("Copy");

  function onBtnClick() {
    navigator.clipboard.writeText(short);
    setBtnText("Copied!");
  }

  function onBtnBlur() {
    setBtnText("Copy");
  }

  //
  const LinkBox = styled("div")(({ theme }) => ({
    display: "flex",
    flexDirection: "column",
    justifyContent: "space-between",
    flexGrow: 1,
    padding: "0 0.5rem",
    [theme.breakpoints.up("sm")]: {
      flexDirection: "row",
    },
  }));

  return (
    <Box sx={{ display: "flex", justifyContent: "space-between", p: "0.5rem", backgroundColor: "white" }}>
      <LinkBox>
        <Link color="neutral" href={original}>
          {original}
        </Link>
        <Link href={short}>{short}</Link>
      </LinkBox>
      <Button variant="solid" onClick={onBtnClick} onBlur={onBtnBlur}>
        {btnText}
      </Button>
    </Box>
  );
}

export default UserLink;
